import argparse
import datetime
import inspect

arg_default = "==+ArgParse+DEFAULT+=="
internal_default = "==+Internal+DEFAULT+=="


class ConfigParser(argparse.ArgumentParser):
    def parse_args(self, args=None, namespace=None):
        args, argv = self.parse_known_args(args, namespace)
        return args


class CommandArg(argparse.Action):
    @property
    def name(self):
        return self.dest

    @name.setter
    def name(self, v):
        self.dest = v

    def add_config(self, config):
        self._config = config
        self.required = self._get_value(self.required, config)
        self.default = self._get_value(self.default, config)
        self.prompt = self._get_value(self.prompt, config)

    def __init__(self, name=None, default=internal_default, required=False,
                 prompt=False, opt_keys=None, prompt_text=None, **kwargs):
        self._config = None
        self._long_opts = []
        self._short_opts = []
        opt_string = self.parse_opt_keys(opt_keys)
        pass_in_default = arg_default
        if not prompt and not required:
            pass_in_default = default
        super().__init__(opt_string, name, default=pass_in_default, required=required, **kwargs)
        self.prompt = prompt
        self.prompt_text = prompt_text
        self.default = default

    def _evaluate(self, argprase_value=None, config=None):
        value = argprase_value
        if value in (None, arg_default):
            default = self._get_value(self.default, config)
            if default == internal_default:
                default = None
            if self._get_value(self.prompt, config):
                input_text = self.prompt_text or "Value needed for {} ({}): ".format(self.name, default or "no default")
                value = input(input_text).strip()
                if value == "":
                    value = None
            value = value or default
        if value in (internal_default, arg_default):
            value = None
        if value is None and self._get_value(self.required, config):
            raise AttributeError("No value give for {} but value is required".format(self.name))
        return value

    def __call__(self, parser, namespace, values, option_string=None):
        if isinstance(values, list) and len(values) == 1:
            values = values[0]
        if len(values) == 0:
            values = None
        value = self._evaluate(values, namespace)
        setattr(namespace, self.dest, value)
        return value

    def _get_value(self, internal_var, config):
        if inspect.isfunction(internal_var):
            return internal_var(config)
        else:
            return internal_var

    def parse_opt_keys(self, opt_keys):
        if isinstance(opt_keys, str):
            opt_keys = (opt_keys,)
        for opt_key in opt_keys or []:
            if len(opt_key) == 1:
                self._short_opts.append(opt_key)
            else:
                self._long_opts.append(opt_key)
        option_strings = []
        for key in self._short_opts:
            option_strings.append("-" + key)
        for key in self._long_opts:
            option_strings.append("--" + key)
        return option_strings


class CommandFlag(CommandArg):
    def __init__(self, *args, **kwargs):
        kwargs["nargs"] = 0
        super().__init__(*args, **kwargs)

    def _evaluate(self, argprase_value=None, config=None):
        return True

    def __call__(self, parser, namespace, values, option_string=None):
        v = str(super().__call__(parser, namespace, values, option_string)).lower()
        setattr(namespace, self.dest, v in ["true", "yes", "y", "1", ""])


class DateRangeCommandArg(CommandArg):
    date_format = "%Y-%m-%d"

    def __init__(self, name=None, default=internal_default, required=False, prompt=False, opt_keys=None,
                 prompt_text=None, **kwargs):
        if isinstance(default, str) and default != internal_default:
            default = [datetime.datetime.strptime(default, self.date_format).date()]

        super().__init__(name, default, required, prompt, opt_keys, prompt_text, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        value = super().__call__(parser, namespace, values, option_string=None)

        dates = value.split(",")
        date_range = []
        for cur_date in dates:
            start, end, *meh = cur_date.split("to") + [None, None]
            start = datetime.datetime.strptime(start, self.date_format).date()
            if end is None:
                date_range.append(start)
                continue
            else:
                end = datetime.datetime.strptime(end, self.date_format).date()
                days = (end - start).days
                date_range.append(str(start))
                for delta in range(1, days):
                    date_range.append(start + datetime.timedelta(delta))
                date_range.append(end)
        setattr(namespace, self.dest, date_range)
        return date_range


class ListCommandArg(CommandArg):
    ALL = "all"
    ALL_VARIATIONS = ("all", "All", "ALL")

    def __init__(self, name=None, default=internal_default, required=False, prompt=False, opt_keys=None,
                 prompt_text=None, delimeter=None, **kwargs):
        if default in self.ALL_VARIATIONS:
            default = self.ALL
        super().__init__(name, default, required, prompt, opt_keys, prompt_text, **kwargs)
        self.delimiter = delimeter or ","

    def __call__(self, parser, namespace, values, option_string=None):
        value = super().__call__(parser, namespace, values, option_string=None)
        if value not in self.ALL_VARIATIONS:
            value = value.split(self.delimiter)
        else:
            value = value.lower()
        setattr(namespace, self.dest, value)
        return value
