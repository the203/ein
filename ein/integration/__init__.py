from contextlib import contextmanager, AbstractContextManager
from dataclasses import dataclass, field
from functools import wraps
from typing import Mapping, Any, Protocol, TypeVar

from compose.extras import LocationInfo
from compose.extras.undef import Undefined, is_defined
from compose import clsname
from ein import logger


def abm(fn):
    @wraps(fn)
    def notimpl(localobj, *args, **kwargs):
        msg = f"{fn.__name__} must be implemented on {clsname(localobj)}.\n\t{LocationInfo(localobj)}"
        raise NotImplementedError(msg)

    return notimpl


class IntegrationInterfacing(Protocol):
    """
        System Integrations and Datasource have an interesting relationship. System Integrations serve as a conduit
        to different types of systems, Snowflake, Oracle, S3, file system etc. They are generally intended to have no
        real knowledge about the Product or data structure that is using the system.  They rely on a datasource to
        know how what to pull from the relivent integration. Thus a datasource needs to have a defined interface to
        different integrations. Integrations can supply the required interface (and documentation) through
        IntegrationInterfacing protocols. Since protocols rooughly follow the concept of Interfaces from static
        languages most subclasses of this should be some sort of -able named class
    """


T_source = TypeVar("T_source", bound=IntegrationInterfacing)


@dataclass
class SystemIntegration(AbstractContextManager):
    _current_item: Any = field(init=False, repr=False)
    _source: T_source = field(init=False, repr=False)

    def source(self, source: T_source = Undefined) -> T_source:
        if is_defined(source):
            self._source = source
        return self._source

    @property
    def count(self):
        try:
            return self.get_count()
        except BaseException:
            return "Unknown"

    @property
    def headers(self) -> Mapping[str, str]:
        return self.extract_headers()

    @abm
    def extract_headers(self):
        raise NotImplementedError("headers needed")

    def get_count(self):
        raise NotImplementedError("need a count")

    def __call__(self, source):
        return self

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.debug("Closing integration down")
        self.close()

    @abm
    def connect(self):
        raise NotImplementedError()

    def close(self):
        raise NotImplementedError()

    def extract(self):
        raise NotImplementedError()
    @abm
    def source_info(self):
        raise NotImplementedError()


@dataclass
class DatabaseIntegration(object):
    _connection = None

    @property
    def connection(self):
        if self._connection is None:
            self._connection = self.make_connection()
        return self._connection

    @contextmanager
    def cursor(self):
        self._cursor = self.connection.cursor()
        yield self._cursor
        self._cursor.close()

    def make_connection(self):
        """Creates whatever connection is needed for integrating to the database

        :return:
        """
        raise NotImplementedError("connection creation us required")
