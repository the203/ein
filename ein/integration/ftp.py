from dataclasses import dataclass
from typing import Text


from ein.outbound.delivery import FileDelivery

from ein import logger
try:
    from pysftp import Connection as BaseConnection


    class SftpConnection(BaseConnection):
        def rename(self, cur, new):
            self._sftp_connect()
            self._sftp.rename(cur, new)

        def mkdir(self, path):
            self._sftp_connect()
            self._sftp.mkdir(path)
except ImportError:
    class SftpConnection(object):
        def __new__(cls, *args, **kwargs):
            logger.warning("SFTP support not available. ")


@dataclass
class SftpDelivery(FileDelivery):
    srv: SftpConnection
    path_to_destination: Text

    add_client_key: bool = True

    def send_file(self, local_file):
        srv = self.srv
        srv.chdir(self.path_to_destination)
        dest_name = local_file
        if self.add_client_key:
            dest_name = self.client_key.lower() + "_" + os.path.basename(dest_name)
        logger.info("put file to %s%s", self.path_to_destination, dest_name)
        srv.put(local_file, remotepath=dest_name)
