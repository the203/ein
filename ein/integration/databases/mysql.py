from dataclasses import asdict, dataclass

import mysql


from ein.integration import DatabaseDelivery, DatabaseIntegration

from ein import logger


@dataclass
class MySqlIntegration(DatabaseIntegration):

    def make_connection(self):
        logger.info("Connecting to MySql...")
        return mysql.connector.connect(**asdict(self))


class MySQLSyncOutboundService(DatabaseDelivery):
    db: MySqlIntegration

    def discover_schema(self):
        with self.cursor() as cursor:
            cursor.execute("SHOW columns FROM " + self.target_schema)
            return dict((column[0], (self.converter_for(column[1]),
                                     all([column[2] == 'NO',
                                          column[4] is None,
                                          column[5] != 'auto_increment']))) for column in
                        cursor.fetchall())
