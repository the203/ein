from dataclasses import dataclass, asdict, field

import json
from typing import Text

from compose import FactoryBundle
from compose.bundling.binding import Bind
from ein import logger
from ein.integration.databases import converter_for, DirectSqlIntegration, SourceQuery
from ein.outbound.delivery import DatabaseDelivery


@dataclass
class SnowflakeConnector(object):
    user: Text 
    password: Text 
    role: Text
    warehouse: Text


    account: Text 
    database: Text
    _connection: 'SnowflakeConnection' = field(init=False, default=None)

    @property
    def connection(self):
        self.connect()
        return self._connection

    def close(self):
        pass

    def connect(self):
        import snowflake.connector as snowflake
        if self._connection is None:
            logger.info("Connecting to Snowflake...")
            snowflake.paramstyle = 'qmark'
            self._connection = snowflake.connect(**asdict(self))
        if self._connection.is_closed():
            self._connection = None
            return self.connect()
        return True

    def cursor(self):
        return self.connection.cursor()


@dataclass
class SnowflakeIntegration(DirectSqlIntegration):
    connector: SnowflakeConnector

    # _connection: 'SnowflakeConnection' = field(init=False, repr=False)

    def connect(self):
        self.connector.connect()

    def close(self):
        self.connector.close()

    def get_count(self):
        self._cursor.count()

    def cursor_for_query(self, query: SourceQuery):
        cursor = self.connector.cursor()
        try:
            cursor.execute(query.qmark, list(query.parameters.values()))
        except  AttributeError:
            cursor.execute(query.qmark, list(query.parameters))
        return cursor


@dataclass
class SnowflakeDelivery(DatabaseDelivery):
    db: SnowflakeConnector

    def discover_schema(self):
        headers = {}
        cur: 'SnowflakeCursor'
        with self.db.connection.cursor() as cur:
            cur.execute("SHOW columns IN " + self.target_schema)
            for column in cur.fetchall():
                required = all([column[5], column[6] is None, column[5] != 'auto_increment'])
                headers[column[2]] = (converter_for(json.loads(column[3])["type"]), required)
        return headers

    def x_prepare(self, selector, **params):
        super().prepare(selector, **params)
        headers = self._commands[1]
        self._commands[1] = "(" + ",".join(f'"{h}"' for h in headers[1:-1].split(",")) + ")"


class SnowflakeBundle(FactoryBundle):
    Bind[SnowflakeConnector].to_self().as_singleton()
