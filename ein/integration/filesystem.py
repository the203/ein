import csv
import json
import os
import re
from dataclasses import dataclass, field
from datetime import datetime as Datetime
from mmap import mmap, ACCESS_READ
from typing import Text, List, Mapping

from compose import Required, clsname
from ein.integration import SystemIntegration, abm
from ..inbound.dialects import EinDialect, mime_csv, open_workbook, xldate_as_tuple, NormalizedFlatFile, JSON


from ein import logger


def utf_8_encoder(unicode_csv_data):
    for line in unicode_csv_data:
        try:
            line = str(line)
        except UnicodeEncodeError:
            line = line.encode("utf-8")
        yield line


class FileParser(object):

    def attach(self, fp):
        self._fp = fp
        return self.iter()

    def iter(self):
        if "__next__" not in dir(self):
            raise NotImplementedError("iter defaults to returning self, so subclasses must implement __next__ "
                                      "or override iter")
        return self

    @property
    def fp(self):
        if self._fp is None:
            raise RuntimeError("File handle accessed before set")
        return self._fp

    def is_supported(self, dialect: EinDialect):
        if self.is_known_dialect(dialect):
            self._dialect = dialect
            return True
        return False

    @abm
    def is_known_dialect(self, dialect: EinDialect) -> bool:
        raise NotImplementedError()


class CsvParser(FileParser):

    def is_known_dialect(self, dialect: EinDialect) -> bool:
        return isinstance(dialect, NormalizedFlatFile)

    def iter(self):
        return csv.reader(utf_8_encoder(x.replace('\0', '') for x in self.fp), dialect=self._dialect)


class JsonParser(FileParser):

    def is_known_dialect(self, dialect: EinDialect) -> bool:
        return isinstance(dialect, JSON)

    def iter(self):
        data = json.load(self.fp)
        if self._dialect.path:
            for node in self._dialect.path.split("."):
                data = data[node]

        if self._dialect.single:
            yield data
        else:
            for di in data:
                yield di


class MsXLParser(FileParser):
    def is_known_dialect(self, dialect: EinDialect) -> bool:
        return hasattr(dialect, "sheet_index")

    def __iter__(self):
        datafile = self.fp
        dialect = self._dialect
        self._book = open_workbook(file_contents=mmap(datafile.fileno(), 0, access=ACCESS_READ))
        self._sheet = self._book.sheet_by_index(dialect.sheet_index)
        self._row_idx = 0
        """
            return (sheet.row_values(row) for row in range(sheet.nrows))
        """

        return self

    def __next__(self):
        row_cells = self._sheet.row_slice(self._row_idx)
        row_values = []
        for cell in row_cells:
            cell_value = cell.value
            if cell.ctype == 3:
                cell_value = Datetime(*xldate_as_tuple(cell_value, self._book.datemode)).isoformat()
            if cell.ctype == 4:
                cell_value = str(True if cell_value == 1 else False)
            row_values.append(cell_value)
        return row_values


class FileFilter(object):
    def is_valid_path(self, file_path):
        return self.is_valid(*os.path.split(file_path))

    def is_valid(self, full_path, file_name):
        raise NotImplementedError()


class TypicalFileFilter(FileFilter):

    def is_valid(self, full_path, file_name):
        return file_name not in ['.DS_Store', 'thumb.db', 'archive']


@dataclass
class FileBasedSystemIntegration(SystemIntegration):
    parser_strategies: List[FileParser] = field(default=Required)
    file_filter: FileFilter = field(default_factory=TypicalFileFilter)
    removal: bool = field(default=False)

    _headings: Mapping[Text, Text] = field(init=False, default=None)
    _current_path: Text = field(init=False, default=None)
    _dialect: EinDialect = field(default=None)

    def get_count(self):
        return "1 billion"

    def source_info(self):
        return self._current_path

    @property
    def dialect(self):
        if self._dialect is not None:
            return self._dialect
        return self._source.mapping.dialect or mime_csv

    @abm
    def get_paths(self) -> List[Text]:
        """Each type of File based integration is expected to use connect (or get_paths) to pull files to a locally
        accessible location and then provide a path to find those files so that can be processed. """
        raise NotImplementedError()

    def extract(self):
        for f_path in self.get_paths():
            yield from self.extract_path(f_path)

    def extract_path(self, data_load_path):
        if os.path.isdir(data_load_path):
            files = [os.path.join(data_load_path, f) for f in next(os.walk(data_load_path))[2]]
        else:
            files = [data_load_path]
        for file_path in files:
            if not self.file_filter.is_valid_path(file_path):
                logger.debug(f"{clsname(self.file_filter)} filtered out {file_path}")
                continue
            logger.info(f"Processing {file_path}")
            self._current_path = file_path
            with open(file_path) as fp:
                yield from self.item_generator(fp)

    def close(self):
        logger.info("Shutttiing stuff down but not really....")
        # self.archive_file(file_name=os.path.basename(file_path))
        # self.clean_up(file_path)

    # def processor_factory(self):
    # return FileProcessor(source_mapping=self.mapping, dialect=self.dialect)
    def extract_headers(self):
        if self._is_keyed:
            return zip(self._current_item.keys(), self._current_item.keys())
        else:
            if self._headings is None:
                self._headings = dict()
                for header in self._current_item:
                    self._headings[re.sub('[^0-9A-Za-z]', "_", header.lower())] = len(self._headings)
            return self._headings

    def archive_file(self, file_name):
        logger.warning("Default file archive does nothing. This message most likely means the file "
                       "processor being used was implemented before archive_file was added, consider refactoring")

    def load_data(self, data_load_path: Text):
        """This method is used to load data into our system

            :param unicode data_load_path: the path where the data to load can be found
        """

    def clean_up(self, file_path):
        if self.removal:
            logger.debug("Removing %s", file_path)
            os.remove(file_path)

    def item_generator(self, fp):
        for parser in self.parser_strategies:
            if parser.is_supported(self.dialect):
                item_iter = parser.attach(fp)
                if self.dialect.firstrowheaders:
                    self._headings = None
                    self._current_item = next(item_iter)
                    self.extract_headers()
                yield from item_iter
                return

        raise NotImplementedError(f"{self.dialect} not supported")

    @property
    def _is_keyed(self):
        try:
            return self.__is_keyed
        except AttributeError:
            self.__is_keyed = hasattr(self._current_item, "keys")
            return self._is_keyed


@dataclass
class LocalIntegration(FileBasedSystemIntegration):
    file_path: Text = None

    def get_paths(self):
        return [self.file_path]

    def connect(self):
        # self._file_pointer = open(self.file_path)
        logger.info(f"sourcing {clsname(self.source())} off filesystem {self.file_path}")
