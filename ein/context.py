import datetime
import logging
import os
import uuid
from dataclasses import dataclass, field
from typing import Text, Optional

from compose import Compose, clsname
from compose.bundling.binding import Bind
from compose.extras import ascls
from compose.modifiers import Lazy
from datamapping import SourceMapping
from ein.job import JobTracker, EinJob
from ein.runtime import RuntimeEnvironment
from ein.util import ensure_dir


@dataclass
class ExecutionContext(Compose):
    tracker: JobTracker = None
    environment: Lazy[RuntimeEnvironment] = None
    start_step: Optional[int] = 0
    start: Optional[datetime.datetime] = datetime.datetime.now()
    transaction_id: Optional[Text] = str(uuid.uuid4())

    _job: EinJob = field(init=False, default=None)

    @property
    def job(self) -> EinJob:
        if self._job is None:
            self._job = self.provide(EinJob)
        return self._job

    def temp_dir(self, sub_system):
        return ensure_dir(os.path.join(self.environment.temp_path, sub_system))

    def stamp(self, item, source_system):
        """This will add additional meta data for each record processed"""
        item.source_system = self.resolve_source_system(source_system)
        item.ein_source = source_system.ein_source
        item.transaction_id = self.transaction_id

    def running(self, step=None, source=None):
        logging.getLogger(clsname(step)).info(f"Running {source}")

    def resolve_source_system(self, step):
        """"""
        if not isinstance(step, type) and isinstance(step, object):
            step = step.__class__
        if isinstance(step, type) and issubclass(step, SourceMapping):
            try:
                step = step._source_system
            except AttributeError as ex:
                msg = "_source_system is missing on {obj}. "
                msg += "This most likely is the result of not using @MapFor on the System Mapping {obj}."
                raise AttributeError(msg.format(obj=step.__name__))
        if isinstance(step, type):
            step = step.__name__
        return step

    def raise_error(self, msg, embedded_error=None, location=None):
        import inspect
        location = ascls(location)

        error_msg = f"""Ein has raised an error. The stack trace accompanying the error is probably not helpful. 
Luckily Ein has provided an alternate location where the issue likely resides.
Error: 
    {msg}
Suggested location to resolve: 
    File "{inspect.getsourcefile(location)}", line {inspect.findsource(location)[1] + 1}, in {clsname(location)}"""
        if embedded_error is None:
            raise RuntimeError(error_msg)
        else:
            logging.getLogger(clsname(location or self)).error(error_msg)
            raise embedded_error

    def error(self, step, ex):
        self.state = "Error"
        logging.error(ex)
        raise ex

    def complete(self, step):
        logging.getLogger(clsname(step)).info("Completed")


ein_context = Compose()
""" Ein Context is not meant to be inherited / extended. App/frameworks should extend the execution 
context Ein's execution and runtime use this context.
 """

with ein_context.registry():
    Bind[JobTracker].to_self()
    Bind[ExecutionContext].to_self().as_singleton()
