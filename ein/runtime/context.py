from compose.proxying import LazyProxy


def load_context():
    from ein.context import ein_context, ExecutionContext
    from ein.outbound.core import OutboundService
    from compose.bundling.binding import Bind

    context = next(ein_context.provide_all(ExecutionContext))
    with context.registry():
        Bind[OutboundService].to_self()
    return context


job_context: 'ExecutionContext' = LazyProxy(load_context)
