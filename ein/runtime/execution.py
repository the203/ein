import os
import sys

if not sys.warnoptions:
    import warnings

    warnings.simplefilter("ignore")

import queue as Queue
from typing import Type

from compose.bundling.binding import Bind
from compose import FactoryBundle

from ein.context import ein_context
from ein.exceptions import ProcessCompletedPreviously
from ein.job import EinJob, StepError
from ein.runtime import get_config, fetch_arg, RuntimeEnvironment

from ein import logger


class JobFailure(Exception):
    pass


def run_job(job_cls: Type[EinJob], config=None, queue=None):
    """This will be used by a job in Ein. Generally speaking what should happen is a job is composed of
    a few parts, an implementation of BaseJob, an implementation of ProcessMediator, and some simple code to
    call this method:

    .. code:: python

        def run():
            run_job(BaseJobImpl)
        if __name__ == '__main__':
            run()

    This method will run the desired job, it will take care of handling common things across all jobs, like running
    for a single institution or running for all institutions. It will also handling loading institution overrides. Jobs
    should make every attempt to avoid not using this, ie USE THIS TO RUN YOUR JOB.

    :param Type job_cls: This should job that should be run.
    :param BaseConfig config: run_job will create the config object but if you must, you can pass one in.
    :param Queue.Queue queue: a Queue for the job to communicate to if it is running in a thread.
    :param dict from str to object job_params: This dictionary will be ** passed to the job upon creation.
    :rtype: None
    """
    # Jobs off main tend to break wiring since wiring might be done for a job and the wiring imports the job
    # from a package not from __main__, so we will try to find the job via package not __main__
    if job_cls.__module__ == "__main__":
        import __main__ as main
        file_name, ext = os.path.splitext(os.path.basename(main.__file__))
        for key in filter(lambda k: file_name in k, sys.modules.keys()):
            if sys.modules[key].__file__ == main.__file__:
                job_cls = sys.modules[key].__getattribute__(job_cls.__name__)

    config_name = os.environ.get("EIN_ENV") or fetch_arg("-c") or fetch_arg("--config")

    with ein_context.registry():
        Bind[RuntimeEnvironment].to(config_name)
        # Bind[EinJob].to(lambda: job_cls)

    from ein.runtime.context import job_context

    logger.info("Creating Job Instance")
    # job_context.job = job_cls
    class EinJobBundle(FactoryBundle):
        #Bind[RuntimeEnvironment].to(config_name)
        Bind[EinJob].to(job_cls)

    job_bundle = EinJobBundle()
    job_cls.bind(job_bundle)
    job_context.register(job_bundle)
    #with job_context.registry() as bundle:
        #Bind[EinJob].to(job_cls)
    #job_cls.bind(bundle)

    l_args = job_context.job.args
    try:
        get_config(job_context, **l_args)
        logger.info("Running Job")
        job_context.job(job_context)
    except StepError as ex:
        # if a step error happened that means it was already handled by the Job. just raise an exception.
        raise SystemExit("Job Failed") from None
    except BaseException as ex:
        logger.error("A job failed")
        raise JobFailure() from ex


def run_task(task: 'DiscreteTask', method="main"):
    """Runs a task, and if given a specific method on the task

    :param BaseConfig config:  config
    :param DiscreteTask task:   system_integration
    :param function or unicode method:  task
    :return: None

    """
    if task is None:
        logger.warning("Null task tried to be run by %s. This may be intentional.", task)
        return 0
    if isinstance(method, str):
        method = getattr(task, method, lambda: NotImplementedError(method))
    try:
        _task_result = method()
        return _task_result
    except ProcessCompletedPreviously:
        logger.info(f"{task.__class__.__name__} completed Previously. It will be skipped")
        return
    except (Exception, BaseException) as ex:
        raise StepError(f"Task Failure: {task}", location=task) from ex
