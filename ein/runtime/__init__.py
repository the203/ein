import argparse
import inspect
import logging
import logging.config
import os
import sys
from types import SimpleNamespace

from compose.extras.undef import Undefined
from compose import clsname
from ein.argparsercontrib import CommandArg


def get_cfg_name():
    return os.environ.get("EIN_ENV")


def config_logging():
    cfg_name = get_cfg_name()
    source_dir = os.path.dirname(inspect.getfile(sys._getframe(3)))
    filename = os.path.join(source_dir, 'logging.' + cfg_name.lower() + '.conf')
    notification = logging.error
    if not os.path.exists(filename):
        filename = os.path.join(source_dir, 'logging.default.conf')
        notification = logging.info
    if not os.path.exists(filename):
        source_dir = os.path.dirname(__file__)
        filename = os.path.join(source_dir, 'logging.default.conf')
        notification = logging.warning
    if not os.path.exists(filename):
        raise Exception("Logging configuration does not exist")
    logging.config.fileConfig(filename, disable_existing_loggers=False)
    notification('Logger Configured based off %s', filename)
    return True


logging_configured = config_logging()


def fetch_arg(param, default=None, args=sys.argv):
    for idx, v in enumerate(args):
        if v == param:
            return sys.argv[idx + 1]
    return default


def get_config(context, **kwargs):
    """This will return any class giving a valid string path, it really is just for Configs to be pased in via
    cmd line though

    ":param String field: the fully qualified Config to be loaded
    :rtype Config: instantiated config
    """

    arg_parser = argparse.ArgumentParser()
    environ = context.environment
    for name, arg in kwargs.items():
        if not isinstance(arg, CommandArg):
            arg = CommandArg(opt_keys=arg)
        if isinstance(arg, CommandArg):
            arg.name = name
            arg.add_config(environ)
            arg_parser._add_action(arg)
    """argparser has no concept of prompting a user in an interactive console, as a result it will raise an error
    for arguments marked as required before we get a chance to prompt a user on the console. As a result we have to
    loop over the arguments and add fake values in the command line to prevent argparser from throwing an error. We do
    this instead of marking them as optional with argparse to continue to give better -h support through argparser"""
    cmd_args = sys.argv[1:]
    for arg in arg_parser._actions:
        if arg.required and arg.prompt and len(set(arg.option_strings).intersection(cmd_args)) == 0:
            cmd_args.append(arg.option_strings[0])
            arg.nargs = 1
            cmd_args.append(Undefined)
    arg_parser.parse_args(args=cmd_args, namespace=context)
    setup_db(environ.connections)


def setup_db(all_connections=None):
    all_connections = all_connections or []
    default = all_connections[0].copy()
    del default["alias"]
    default["db"] = default["name"]
    del default["name"]
    import mongoengine
    mongoengine.connect(**default)

    for connection_info in all_connections:
        mongoengine.register_connection(tz_aware=True, **connection_info)


def connections(host="localhost", port=27017, replicaSet=None, read_preference=None, db_prefix="", **kwargs):
    if not host.startswith("mongodb://"):
        host = "mongodb://{}:{}".format(host, port)

    if not host.endswith("/{}") and "{}" not in host:
        host += "/{}"
    dsa_name = kwargs.pop('dsa', 'dsa')
    connections = [
        {"alias": dsa_name,
         "name": dsa_name,
         "host": host.format(db_prefix + dsa_name)},
        {"alias": 'logging',
         "name": "logging",
         "host": host.format(db_prefix + 'logging')}
    ]
    if replicaSet is not None:
        kwargs.update(dict(replicaSet=replicaSet, read_preference=read_preference))
    if len(kwargs) > 0:
        for connection in connections:
            connection.update(kwargs)

    return connections


class RuntimeEnvironment(SimpleNamespace):
    log_path = "/apps/logs/"
    temp_path = "/tmp"
    debug_sql = False
    connections = connections()

    xl_connection = {"driver_name": "ODBC Driver 11 for SQL Server", "host": "bridge.qa.nada.sh", "port": "21401",
                     "database": "deltanetsioa", "uid": "app_activityfeeds",
                     "passwd": "t5MN9mzQHq2rHUGLkg"}

    @property
    def is_local(self):
        return clsname(self).lower().startswith("local")
