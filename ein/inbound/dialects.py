import csv
from dataclasses import dataclass
from typing import Text

from ein import logger

EXCEL_SUPPORT = False


@dataclass
class EinDialect(object):
    firstrowheaders: bool = True
    encoding: Text = "utf-8"


@dataclass
class NormalizedFlatFile(EinDialect):
    delimiter: Text = ','
    quotechar: Text = '"'
    escapechar: Text = None
    skipinitialspace: bool = False
    lineterminator: Text = '\r\n'
    doublequote: bool = True
    quoting: Text = csv.QUOTE_MINIMAL


mime_csv = NormalizedFlatFile()
tab_csv = NormalizedFlatFile(delimiter="\t", encoding="utf-16")
headerless_csv = NormalizedFlatFile(firstrowheaders=False)

try:
    from xlrd import open_workbook, xldate_as_tuple
    from xlrd.sheet import Cell
except ImportError:
    logger.warning("Excel Support not installed")
    open_workbook = None
    xldate_as_tuple = None
    xlsx = EinDialect()
else:
    EXCEL_SUPPORT = True


    @dataclass
    class MsXlsx(EinDialect):
        sheet_index: int = 0

    xlsx = MsXlsx()

@dataclass
class JSON(EinDialect):
    path: Text = ''
    single: bool = False
