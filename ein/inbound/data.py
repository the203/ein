from itertools import chain
from typing import List, Protocol, Text

from mongoengine import DynamicDocument, Document, fields, EmbeddedDocument, BulkWriteError, ValidationError
from pymongo import UpdateOne

from compose.extras.undef import default, Undefined
from ein import logger
from ein.util import convert_to


class HashSave(Document):
    """
    SafeSave is a simple base(abstract) class intended to be used generally in a multiple inheritance way. The class
    is meant to give a nice standard way to ensure duplicate records are not added to the database. It also lets the
    system focus on getting data ready and handles the save/update question.

    NOTE: that it does take a performance hit since it pre-checks for a record on each save.

    If a class inherits off of this it MUST implement the hash as a property.
    """
    item_hash = fields.StringField()

    meta = {
        "indexes": ["item_hash"],
        "abstract": True
    }

    def hash(self):
        raise NotImplementedError()


class SafeSaveEmbeddable(EmbeddedDocument):
    item_hash = fields.StringField()
    meta = {
        "abstract": True
    }

    # @abstract_property
    def hash(self):
        pass

    def save(self, **kwargs):
        doc_hash = self.hash
        self.item_hash = doc_hash
        item = self.__class__.objects(item_hash=self.hash).first()
        if item is not None:
            self.id = item.id
        return super(SafeSaveEmbeddable, self).save(**kwargs)


class EnforceUnique(HashSave):
    item_hash = fields.StringField(unique=True)

    meta = {
        "abstract": True
    }

    def save(self, **kwargs):
        doc_hash = self.hash
        self.item_hash = doc_hash
        return super(EnforceUnique, self).save(**kwargs)


class SafeSave(HashSave):
    item_hash = fields.StringField(unique=True)

    meta = {
        "abstract": True
    }

    def should_save(self, item):
        """ This should rarely be used but might be needed for time sensative information that syncs across systems
        :param item: the item currently in the database
        :return: boolean
        """
        return True

    def save(self, **kwargs):
        doc_hash = self.hash
        self.item_hash = doc_hash
        item = self.__class__.objects(item_hash=self.hash).first()
        if item is not None:
            if self.should_save(item):
                self.id = item.id
            else:
                return item
        try:
            return super(SafeSave, self).save(**kwargs)
        except ValidationError as ex:
            print(self.to_json())
            raise


class Embeddable(object):

    def embeddable_type(self):
        raise NotImplementedError("No embedded type is available for this data collection")

    def convert_to_embeddable(self):
        return convert_to(self, self.embeddable_type())


class DataCollection(object):
    @classmethod
    def name(cls):
        return cls.__name__

    @classmethod
    def __ignored_fields(cls) -> List[Text]:
        """A list of fields that are ignored when serializing items from the collection to send
        through a deliver service (this should not impact persistence of the object). Generally this
        should be a list of internally used attributes that don't add value to the data.

        :return:
        """
        raise NotImplementedError("__ignored fields is needed")

    @classmethod
    def pii_fields(cls):
        return {"first_name", "last_name", "student_email"}

    def do_not_serialize(self):
        ignore_list = []
        for super_cls in type(self).mro()[:-2]:
            ignore_list += super_cls.ignored_fields(self)

        return ignore_list

    def ignored_fields(self):
        """A list of fields that are ignored when serializing items from the collection to send
                through a deliver service (this should not impact persistence of the object). Generally this
                should be a list of internally used attributes that don't add value to the data.

                :return:
                """
        raise NotImplementedError("ignored_fields fields is needed")


class MongoDataCollection(DynamicDocument, SafeSave, DataCollection):
    ein_source: Text = fields.StringField()
    source_system: Text = fields.StringField()
    meta = {
        "abstract": True
    }

    def ignored_fields(self):
        return ["item_hash", "_id", "source_file", "source_system", "_cls"]

    @classmethod
    def api(cls, **params):
        for k, v in list(params.items()):
            if isinstance(v, (list, set)):
                if len(v) > 1:
                    del params[k]
                    params[k + "__in"] = v
                else:
                    v = v[0]
                    params[k] = v
            if "," in v and v[0] != '"':
                del params[k]
                params[k + "__in"] = v.split(",")
        logger.debug("Querying {} with {}".format(cls.name(), params))
        excluded_fields = MongoDataCollection.ignored_fields(cls, *cls._ignored_fields)
        return cls.objects(**params).exclude(*excluded_fields).no_cache()

    @classmethod
    def bulk_save(cls, data: List['MongoDataCollection'],
                  upsert=True, validate_insert=True, pre=Undefined,
                  post=Undefined):
        """Short cut to bulk save on data class

        :param data: the data to be bulk saved
        :param id_projection:
        :param upsert: insert or update
        :return:
        """
        # make sure it really is a list
        data = list(data)
        pre = default(pre, {})
        post = default(post, {})
        writes = {}
        id_projection = cls.get_projected()

        new_items = {}
        if len(data) > 0:
            for idx, item in enumerate(data):
                writes.setdefault(item.__class__, [])
                new_items.setdefault(item.__class__, 0)

                update_doc = item._get_update_doc()
                if len(update_doc) == 0:
                    continue
                if item.id is None:
                    new_items[item.__class__] += 1

                writes[item.__class__].append((idx, UpdateOne(id_projection(item), update_doc, upsert=upsert)))
            for data_cls, writes_for_cls in writes.items():
                expected_inserts = new_items[data_cls]
                if len(writes_for_cls):
                    try:
                        bulk = chain(pre.get(data_cls, []), (w for i, w in writes_for_cls), post.get(data_cls, []))
                        result = data_cls.objects()._collection.bulk_write(list(bulk))
                        actual_inserts = result.upserted_count + result.inserted_count
                        if validate_insert and actual_inserts != expected_inserts:
                            raise RuntimeError(f"bulk save expected {expected_inserts} new documents to be created "
                                               f"only {actual_inserts} were reported as inserted. "
                                               f"This likely means items exist in the database already but are not "
                                               f"being selected by the controller. Does this feed include inactive"
                                               f"entries while the controller only loads active entries?")
                        for idx, id in result.upserted_ids.items():
                            data[writes_for_cls[idx][0]].id = id
                    except BulkWriteError as ex:
                        logger.error(ex)
                        raise
            return True

    @classmethod
    def get_projected(cls):
        def id_projection(i):
            return {"item_hash": i.item_hash}

        return id_projection
