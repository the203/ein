import os
from dataclasses import dataclass, field
from typing import List

from datamapping import BadEntryException
from ein.base.tasks import DataItemPostProcessTask
from ein.integration import SystemIntegration
from ein.runtime.context import job_context
from ein.util import ensure_dir


from ein import logger
import datamapping


class BaseSourceLinker(DataItemPostProcessTask):
    priority: int = 2

    @property
    def data_names(self):
        return []

    def augment_data_type(self, cls):
        pass

    def update_item(self, item):
        """ Update a source item

        :param DataCollection item: some sort of DataCollection
        :return: Nothing
        """


class StampItem(DataItemPostProcessTask):
    def cares_about(self, data_source):
        return True

    def update_item(self, item, source):
        from ein.runtime.context import job_context
        job_context.stamp(item, source_system=source)


@dataclass
class EinDataSource(DiscreteTask):
    integration: SystemIntegration
    is_primary: bool = field(default=True)
    post_processes: List[DataItemPostProcessTask] = field(default_factory=list)

    @property
    def mapping(self):
        return datamapping.locate(self)

    def data_collection(self):
        return self.mapping.target_collection

    def __post_init__(self):
        self._buffer = []
        # TODO where does this go now?
        self.related_sources = list(getattr(self, "related_sources", []))
        for related_course_cls in getattr(self.__class__, "_related_sources", []):
            related_source = related_course_cls()
            related_source.is_primary = self.is_primary
            related_source.source_linker = self.source_linker
            self.related_sources.append(related_source)

    def main(self):
        """
        This is an important method. Describe it.
        :return:
        """
        _post_tasks = []
        for task in self.post_processes:
            if task.cares_about(self.data_collection()):
                _post_tasks.append(task)
        self.post_processes = sorted(_post_tasks, key=lambda x: x.priority)
        self.integration.source(self)
        with self.integration as integration:
            for item in integration.extract():
                self.process_item(item)
        print(f"Saving {len(self._buffer)}")
        for i in self._buffer:
            i.save()


    def store(self, item):
        self._buffer.append(item)

    def get_download_path(self, prefix="load", added_path="", ):
        file_name = "{}-{}.csv".format(prefix, self._process_manager.get_run_discriminator())
        return os.path.join(self.get_download_dir(added_path=added_path), file_name)

    def get_download_dir(self, added_path=""):
        return ensure_dir(os.path.join(self._config.temp_dir(self.__class__.__name__), self._config.client_key,
                                       added_path))

    @property
    def ein_source(self):
        return self.integration.source_info()

    def process_item(self, row):
        self._current_row = row
        try:
            item = self.mapping().map_item(row, headings=self.integration.headers)
            self.fire_post_process(item)
            self.store(item)
        except BadEntryException as beex:
            if hasattr(beex, 'message'):
                if beex.message not in ("", None):
                    logger.debug("Bad entry ignored, reason:" + beex.message)

    def finished(self):
        job_context.complete(step=self.source_system)

    def fire_post_process(self, item):
        for task in self.post_processes:
            task.update_item(item, self)
