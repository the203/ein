import random
import time
from functools import partial, wraps



from ein import logger

DEFAULT_TRIES = 10
DEFAULT_DELAY = 0
DEFAULT_MAX_DELAY = None
DEFAULT_BACKOFF = 1.1
DEFAULT_JITTER = None
DEFAULT_EXCEPTIONS = (Exception,)


def till_success(f, exceptions=DEFAULT_EXCEPTIONS, tries=DEFAULT_TRIES, delay=DEFAULT_DELAY,
                 max_delay=DEFAULT_MAX_DELAY, backoff=DEFAULT_BACKOFF, jitter=DEFAULT_JITTER):
    """
    Executes a function and retries it if it failed.

    :param f: the function to execute.
    :param exceptions: an exception or a tuple of exceptions to catch.
    :param tries: the maximum number of attempts. negative value will result in infinite tries.
    :param delay: initial delay between attempts.
    :param max_delay: the maximum value of delay.
    :param backoff: multiplier applied to delay between attempts.
    :param jitter: extra seconds added to delay between attempts range tuple (min, max) skipped if None
    :returns: the result of the f function.
    """
    if tries:
        tries += 1
    _tries, _delay = tries, delay
    while _tries:
        try:
            return f()
        except exceptions as e:
            _tries -= 1
            if not _tries:
                raise

            if logger is not None:
                logger.warning('%s, retrying in %s seconds...', e, _delay)

            time.sleep(_delay)
            _delay *= backoff

            if jitter:
                _delay += random.uniform(*jitter)

            if max_delay is not None:
                _delay = min(_delay, max_delay)


def resilient(exceptions=DEFAULT_EXCEPTIONS, tries=DEFAULT_TRIES, delay=DEFAULT_DELAY,
              max_delay=DEFAULT_MAX_DELAY, backoff=DEFAULT_BACKOFF, jitter=DEFAULT_JITTER):
    """Returns a retry decorator.

    :param f: the function to execute.
    :param exceptions: an exception or a tuple of exceptions to catch.
    :param tries: the maximum number of attempts. negative value will result in infinite tries.
    :param delay: initial delay between attempts.
    :param max_delay: the maximum value of delay.
    :param backoff: multiplier applied to delay between attempts.
    :param jitter: extra seconds added to delay between attempts range tuple (min, max) skipped if None
    :returns: the decorator for function.
    """

    return partial(retry, exceptions=exceptions, tries=tries, delay=delay, max_delay=max_delay
                   , backoff=backoff, jitter=jitter)


def retry(f, exceptions=DEFAULT_EXCEPTIONS, tries=DEFAULT_TRIES, delay=DEFAULT_DELAY,
          max_delay=DEFAULT_MAX_DELAY, backoff=DEFAULT_BACKOFF, jitter=DEFAULT_JITTER):
    """
    Decorator for a funtion, all values are defaults if this is used directly as a decorator.
    To change the defaults either alter the constants or use resilient

    :param f: the function to execute.
    :param exceptions: an exception or a tuple of exceptions to catch.
    :param tries: the maximum number of attempts. negative value will result in infinite tries.
    :param delay: initial delay between attempts.
    :param max_delay: the maximum value of delay.
    :param backoff: multiplier applied to delay between attempts.
    :param jitter: extra seconds added to delay between attempts range tuple (min, max) skipped if None
    :returns: the result of the f function.
    """
    wrapped = wraps(f)(partial(till_success, f, exceptions=exceptions,
                               tries=tries, delay=delay, max_delay=max_delay, backoff=backoff, jitter=jitter))

    return wrapped
