from dataclasses import dataclass
from typing import Type

from ein import logger
from ein.base.services import BaseService
from ein.base.tasks import DiscreteTask
from ein.inbound.data import MongoDataCollection
from ein.serializers.base import BaseSerializer


@dataclass
class BaseDelivery(DiscreteTask):
    serializer: BaseSerializer

    def prepare(self, selector:'ItemSelector'):
        pass

    def accept(self, item):
        raise NotImplementedError("Accept must be implemented by Deliveries")

    def finished(self):
        pass


class ItemSelector(object):

    @property
    def items(self):
        return self.get_items()

    def get_items(self):
        raise NotImplementedError("items must be implemented for  ItemSelectors ")

    def count(self):
        return None


@dataclass
class GenericItemSelector(ItemSelector):
    target: Type[MongoDataCollection]

    def get_items(self):
        return self.target.objects()

    def count(self):
        return self.items.count()


@dataclass
class OutboundService(BaseService):
    delivery: BaseDelivery
    selector: ItemSelector

    def __post_init__(self):
        self._items = []

    def run(self):
        logger.info("Preparing Delivery")
        self.delivery.prepare(self.selector)
        logger.info("Finding Items to Serialize")
        logger.debug(f"{self.count()} items will be sent to the external service")
        for item in self.selector.items:
            self.send(item)
        self.delivery.finished()

    def send(self, item):
        self.delivery.accept(item)

    def count(self):
        return self.selector.count()
