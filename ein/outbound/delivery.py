import os
import shutil
import time
from dataclasses import dataclass, field
from io import BufferedReader
from typing import List, Text, Mapping

from mongoengine import fields

from compose import clsname
from ein.integration import DatabaseIntegration

from ein.resilient import resilient
from ein.outbound.core import BaseDelivery
from ein.runtime.context import job_context
from ein.serializers.sepcialized import DatabaseSerializer
from ein.util import ensure_path_exists

from ein import logger


@dataclass
class ServiceDelivery(BaseDelivery):

    def __post_init__(self):
        self.attempts = 0

    def accept(self, item):
        try:
            if self.should_send_item(item):
                serialized_item = self.serializer.serialize(item)
                logger.info(f"Sending item via {clsname(self)}")
                self.send_item(serialized_item, original=item)
                self.attempts = 0
                logger.info("File has been sent")
        except BaseException as ex:
            if self.attempts >= 4:
                raise
            delay = (60 * self.attempts) + 15
            logger.error(ex)
            logger.error("Service call failed, will try again in %s seconds", delay)
            self.attempts += 1
            time.sleep(delay)
            self.accept(item)

    def should_send_item(self, item):
        return True

    def send_item(self, payload, original=None):
        pass


@dataclass
class FileDelivery(BaseDelivery):
    _values: List[Text]

    def local_override_enabled(self):
        return True

    @property
    def local_override(self):
        return self.local_override_enabled() and getattr(job_context.config, "local_override", False)

    def accept(self, item):
        row = self.serializer.serialize(item)
        self._values.append(row)

    @resilient()
    def finished(self):
        file_pointer = self.temp_file()
        if self.local_override:
            logger.warn("Local config run detected, NOT delivering file as configured.")
            LocalDelivery().send_file(file_pointer)
            logger.info("File has been sent")
            return
        logger.info("Sending file via {clsname(self)}")
        self.send_file(file_pointer)
        logger.info("File has been sent")
        self.finished()

    def temp_file(self) -> BufferedReader:
        raise NotImplementedError("Must write the file to some local please,")


class FolderDelivery(FileDelivery):
    path_to_destination = fields.StringField()
    """:type: str """

    def send_file(self, local_file):
        ensure_path_exists(self.path_to_destination)
        destination = os.path.join(self.path_to_destination, os.path.basename(local_file))
        try:
            os.remove(destination)
        except Exception:
            pass
        logger.info("File Destination: " + destination)
        shutil.move(local_file, destination)


class LocalDelivery(FolderDelivery):
    path_to_destination = "/opt/app/dw_files"


class SharedFolderDelivery(FileDelivery):
    path_to_destination = fields.StringField()
    """:type: str """

    def send_file(self, local_file):
        ensure_path_exists(self.path_to_destination)
        file_name = self.client_key + "-" + os.path.basename(local_file)
        destination = os.path.join(self.path_to_destination, file_name)
        try:
            os.remove(destination)
        except Exception:
            pass
        shutil.move(local_file, destination)


@dataclass
class DatabaseDelivery(BaseDelivery):
    target_schema: Text
    db: DatabaseIntegration
    serializer: DatabaseSerializer

    _headers: Mapping[Text, tuple] = field(init=False, default_factory=lambda: {})
    _commands: List[Text] = field(init=False, default_factory=list)
    _values: List[Text] = field(init=False, default_factory=list)

    def discover_schema(self):
        raise NotImplementedError("Schema Discovery is  needed")

    def prepare(self, selector, **params):
        self._headers = self.discover_schema()
        self.serializer.configure(self._headers)
        self._commands = [f"""INSERT INTO {self.target_schema}""",
                          f"""({','.join(self._headers.keys())})""", "VALUES"]

    def accept(self, item):
        row = self.serializer.serialize(item)
        self._values.append(row)

    def finished(self):
        self._commands.append(",\n".join(self._values))
        command = "\n".join(self._commands)
        with self.db.connection.cursor() as cur:
            # cur.execute(table_create)
            logger.info(command)
            cur.execute(command)
