import codecs
import datetime
import json
import numbers
import os
import re
import shutil
from mongoengine import fields
from mongoengine.base.fields import BaseField
from xml.etree import cElementTree as et








num_match = re.compile("^[0-9,\.]+$")


class FlatFileSerializer(BasePiiSerializer, FileBasedMixin):
    headings = fields.StringField()
    """:type: unicode"""

    @property
    def writer(self):
        if self._writer is None:
            self._writer = self.make_writer()
        return self._writer

    def __init__(self, *args, **kwargs):
        super(FlatFileSerializer, self).__init__(*args, **kwargs)
        if self.headings is not None:
            self._headings = self.headings.split(",")
        else:
            logger.warn("%s created without heading information.", self.__class__.__name__)
        self._writer = None
        self.file_stream = None
        """:type: File"""

    @abstract
    def writerow(self, *values):
        pass

    @abstract
    def make_writer(self):
        pass

    def initialize(self):

        self._writer = self.make_writer()
        self.writerow(*self._headings)

    def is_numeric(self, value):
        uni_value = str(value)
        return any([isinstance(value, numbers.Number),
                    uni_value.isnumeric(),
                    uni_value.isdecimal(),
                    (num_match.findall(uni_value) + ["....."])[0].count(".") <= 1
                    ])

    def transform(self, value):
        if self.is_numeric(value):
            try:
                value = value.replace(",", "")
            except AttributeError:
                pass
            value = float(value)

            if value == int(value):
                value = int(value)
        elif not isinstance(value, str):
            value = str(value)

        return value

    def serialize(self, item):
        row_data = []
        for header in self._headings:
            value = traverse_path(header, item, "")
            """ check to see if got a value, if not may be in enrollment section """
            if value == "":
                enrollment = item.get("enrollment")
                if enrollment:
                    value = enrollment.get(header, "")
                    value = self.transform(value)
            else:
                value = self.transform(value)

            row_data.append(value)
        try:
            self.writerow(*row_data)
        except BaseException:
            logger.info(",".join(str(s) for s in row_data))
            raise

    @classmethod
    def make_from(cls, data_cls, ignore=None, exclude_pii=True):
        ignore = (ignore or []) + list(cls.ignored_fields_set(data_cls, exclude_pii=exclude_pii))
        headings = []
        for field_name in data_cls._fields_ordered:
            if isinstance(getattr(data_cls, field_name, None), BaseField) and field_name not in ignore:
                headings.append(field_name)
        return cls(headings=",".join(headings), exclude_pii=exclude_pii)


class CsvSerializer(FlatFileSerializer):
    dialect = fields.StringField(default=lambda: "mssql")

    def __init__(self, *args, **kwargs):
        super(CsvSerializer, self).__init__(*args, **kwargs)
        self._base_file_name = "activity"

    @property
    def base_file_name(self):
        return self._base_file_name

    @base_file_name.setter
    def base_file_name(self, value):
        self._base_file_name = value

    def make_writer(self):
        self.file_stream = open(self.temp_path, 'wb')
        return csv.writer(self.file_stream, dialect=self.dialect)
        """:type: csv.writer"""

    def writerow(self, *values):
        self.writer.writerow(values)

    def write(self):
        self.file_stream.close()
        return self.temp_path

    def extension(self):
        return "csv"


class XlsxSerializer(FlatFileSerializer):
    _wb = None

    def make_writer(self):
        self._wb = Workbook()
        ws1 = self._wb.active
        ws1.title = "data"
        return ws1

    def writerow(self, *values):
        self.writer.append(values)

    def write(self):
        self._wb.save(filename=self.temp_path)
        return self.temp_path

    def extension(self):
        return "xlsx"


#
# The below serializers are older and hard to subclass. They work perfectly well but if you need a new serializer
# you are best to focus on subclassing from above classes only.

class FileBasedSerializer(BaseSerializer):
    include_missing_enrollment = fields.BooleanField(default=False)

    def __init__(self, *args, **kwargs):
        super(FileBasedSerializer, self).__init__(*args, **kwargs)
        self.json_dest = None

    @abstract_property
    def base_file_name(self):
        pass

    def add_item(self, item, is_last):
        self.json_dest.write(self.serialize(item))
        if not is_last:
            self.json_dest.write(",")
        self.json_dest.flush()

    @property
    def temp_path(self):
        return "{}/{}_{}.{}".format(self.config.temp_dir("outbound"), self.base_file_name,
                                    self.process_manager.get_run_discriminator(),
                                    self.extension())

    @property
    def stream_path(self):
        return "{}/{}_{}.{}".format(self.config.temp_dir("outbound"), self.base_file_name,
                                    self.process_manager.get_run_discriminator(),
                                    "stream")

    def write(self):
        local_file = self.temp_path
        logger.info("Local path {}".format(local_file))
        try:
            os.remove(local_file)
        except Exception:
            pass
        local_file = self.write_to_path(local_path=local_file) or local_file
        logger.info("File Created: {}".format(local_file))
        return local_file

    @abstract
    def write_to_path(self, local_path):
        pass

    @abstract
    def extension(self):
        pass




class GenericXmlSerializer(GenericJsonSerializer):
    def write_to_path(self, local_path):
        super(GenericXmlSerializer, self).write_to_path(local_path)
        logger.info("Converting JSON to XML")
        return convert_json("data", local_path)
