import logging

from mongoengine import fields
from mongoengine.base import get_document

from ein.outbound.core import ItemSelector

from ein import logger


class TransactionSelector(ItemSelector):
    item_cls_name = fields.StringField()

    def find_items(self):
        _cls = get_document(self.item_cls_name)
        return _cls.objects(transaction_id=self._process_manager.transaction_id)

    def configure(self, _cls):
        self.item_cls_name = _cls._class_name
        return self

    @classmethod
    def create(cls, _cls):
        s = cls()
        s.configure(_cls)
        return s


class ActivityQuery(ItemSelector):
    def find_items(self):
        return self.get_activities()

    def get_activities(self):
        """

         :rtype: list of StudentActivity
        """
        from amadou.activity import StudentActivity
        qry = StudentActivity.objects(transaction_id=self.process_manager.transaction_id)
        logger.info(qry._query)
        return StudentActivity.objects(transaction_id=self.process_manager.transaction_id)
