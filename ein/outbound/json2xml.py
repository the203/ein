import codecs
import os

import json
import numbers

from xml.dom.minidom import Document


def parse_element(doc, root, j):
    if isinstance(j, dict):
        for key in list(j.keys()):
            value = j[key]
            if isinstance(value, list):
                for e in value:
                    elem = doc.createElement(key)
                    parse_element(doc, elem, e)
                    root.appendChild(elem)
            else:
                if key.isdigit():
                    elem = doc.createElement('item')
                    elem.setAttribute('value', key)
                else:
                    elem = doc.createElement(key)
                parse_element(doc, elem, value)
                root.appendChild(elem)
    elif isinstance(j, str) or isinstance(j, str):
        text = doc.createTextNode(j)
        root.appendChild(text)
    elif isinstance(j, numbers.Number):
        text = doc.createTextNode(str(j))
        root.appendChild(text)
    else:
        raise Exception("bad type '%s' for '%s'" % (type(j), j,))


def convert_json_obj(root, j):
    doc = Document()
    if root is None:
        if len(list(j.keys())) > 1:
            raise Exception('Expected one root element, or use --root to set root')
        root = list(j.keys())[0]
        elem = doc.createElement(root)
        j = j[root]
    else:
        elem = doc.createElement(root)
    parse_element(doc, elem, j)
    doc.appendChild(elem)
    return doc


def convert_json_file(root, json_path):
    with codecs.open(json_path, 'r') as js_fobj:
        j = json.load(js_fobj)
    extension = json_path.split(".")[-1]
    doc = convert_json_obj(root, j)
    xml_path = json_path.replace(extension, "xml")
    with codecs.open(xml_path, 'w') as xml_fobj:
        doc.writexml(xml_fobj)
    os.remove(json_path)
    return xml_path


def convert_json(root, json):
    if os.path.isfile(json):
        return convert_json_file(root, json)
    else:
        return convert_json_obj(root, json)