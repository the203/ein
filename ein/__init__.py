import datetime
import logging
import os

logger = logging.getLogger("ein")
if True:
    from compose.extras.logging import logger

    logger = logger


class JobLogHandler(logging.FileHandler):
    def __init__(self, lpath, base_name):
        self.lpath = lpath
        self.base_name = base_name
        self.log_creation_time = datetime.datetime.now()
        filename = self.make_file_name()
        super().__init__(filename)

    def expire_old_Logs(self, shelflife):
        shelflife = shelflife or datetime.timedelta(days=4)
        right_now = datetime.datetime.now()
        for count, fpath in enumerate(reversed(glob.glob(os.path.join(self.lpath, self.base_name) + "*"))):
            if (right_now - datetime.datetime.fromtimestamp(os.path.getmtime(fpath))) > shelflife or count > 10:
                try:
                    os.remove(fpath)
                except FileNotFoundError:
                    pass  # If we try to remove it and it isn't there... move on

    def make_file_name(self):
        time_appender = self.log_creation_time.isoformat()[:-7]
        filename = self.file_name(time_appender)
        if os.path.exists(filename):
            filename = self.file_name(time_appender, str(self.log_creation_time.isoformat()[-6:]))
        return filename

    def file_name(self, *args):
        file_name_appenders = list(args)
        file_name_appenders.insert(0, self.base_name)
        return os.path.join(self.lpath, "-".join(file_name_appenders) + '.log')
