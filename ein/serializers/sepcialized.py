from dataclasses import field, dataclass

import datetime
from typing import Mapping, Text, MutableMapping, Tuple, Callable

from ein import logger
from ein.integration.databases import sql_escape
from ein.serializers.base import BaseSerializer
from ein.util import flatten


@dataclass
class DatabaseSerializer(BaseSerializer):
    schema_mapping: Mapping[Text, Text] = field(default_factory=dict)

    _headers: MutableMapping[Text, Tuple[Callable[[Text], Text], bool]] = field(init=False, default_factory=dict)

    def configure(self, headers):
        self._headers = headers

    def prepare_item(self, item):
        return flatten(super().prepare_item(item))

    def serialize_item(self, item):
        row = {}
        for header, (converter, required) in self._headers.items():
            header = header.lower()
            item_path = self.schema_mapping.get(header, header)
            split_path = item_path.split(".")
            idx = None
            if split_path[-1].isnumeric():
                idx = int(split_path.pop())
                item_path = ".".join(split_path)
            val = item.get(item_path, None)
            if header == "sync_date" and val is None:
                val = datetime.date.today()
            if header == "sync_key":
                val = item.id
            if idx is not None and hasattr(val, "__getitem__"):
                if len(val) > idx:
                    val = val[idx]
                else:
                    val = None
                    logger.warning(f"idx of {idx} out of range for {val} from {item_path}")
            if val is not None:
                val = converter(val)
            if required and val is None:
                raise ValueError(f"{header} is a required field")
            try:
                row[header] = sql_escape(val)
            except:
                logger.error(f"{header}: {val} unable to be prepared fror SQL, {item}")
        return "(" + ",".join(row.values()) + ")"
