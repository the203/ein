from dataclasses import asdict, dataclass, field
from itertools import chain
from typing import List, Text

from ein import logger
from ein.inbound.data import MongoDataCollection
from ein.base.tasks import DiscreteTask


@dataclass
class BaseSerializer(DiscreteTask):
    excluded_fields: List[Text] = field(default_factory=list)

    @property
    def remove_fields(self):
        return chain(self.excluded_fields)

    def prepare_item(self, item):
        try:
            return item.to_mongo()
        except AttributeError:
            return asdict(item)

    def serialize(self, item):
        added_ignore = []
        try:
            added_ignore = item.do_not_serialize()
        except AttributeError:
            pass
        except:
            logger.warning("Do Not serialize list threw and error.")
        item = self.prepare_item(item)

        for ignored_field in chain(self.excluded_fields, added_ignore):
            item.pop(ignored_field, None)

        return self.serialize_item(item)

    def serialize_item(self, item):
        raise NotImplementedError("serializers need to be serializing")

    @staticmethod
    def traverse_path(key_path, item, default=""):
        """Walk the tree of the keypath and get the value, written to be compliant with flatdict key structure

        :param key_path: the path to travel down into the dict to find the value
        :param item: the dict (or dict like) object that has values
        :param default: if, at any time, traversing the path nothing is found default is returned
        :return:
        """
        not_found_hash = "6ee0e45d-b459-4c6e-b49d-48f80de0309e"
        if isinstance(key_path, str):
            key_path = key_path.split(":")
        for key in key_path:
            try:
                item = item.get(key, not_found_hash)
            except AttributeError:
                # Must be a list-ish type object try notation
                try:
                    item = item[int(key)]
                except (ValueError, IndexError):
                    return default
            if item == not_found_hash:
                return default
        return item


@dataclass
class BasePiiSerializer(BaseSerializer):
    exclude_pii: bool = True
    """:type:boolean"""

    @classmethod
    def ignored_fields_set(cls, item_cls, exclude_pii=True):
        ignored_fields = set(["client_key", "transaction_id"])
        ignored_fields.update(MongoDataCollection.ignored_fields(item_cls, *MongoDataCollection._ignored_fields))
        if exclude_pii:
            ignored_fields.update(item_cls.pii_fields())
        return ignored_fields

    def add_item(self, item, is_last):
        """Add item to serialization

        :param MongoDataCollection item:  item
        :param Boolean is_last: last or not
        :return: path
        """
        try:
            item_dict = item.to_mongo()
        except AttributeError:
            item_dict = asdict(item)

        for ignored_field in self.excluded_fields:
            item_dict.pop(ignored_field, None)

        if "enrollment" in item_dict:
            if not isinstance(item_dict["enrollment"], dict):
                item_dict["enrollment"] = item_dict["enrollment"].to_mongo()
            from amadou.enrollment import EmbeddedEnrollmentRecord
            ignored_fields = MongoDataCollection.ignored_fields(EmbeddedEnrollmentRecord, *MongoDataCollection._ignored_fields)
            if self.exclude_pii:
                ignored_fields.update(EmbeddedEnrollmentRecord.pii_fields())
            for ignored_field in ignored_fields:
                item_dict["enrollment"].pop(ignored_field, None)
        self.serialize(item_dict)

    # @abstract
    def serialize(self, item):
        """Unlike Add Item. This item should be a dict. It also will (or should) have all PII removed if the serializer
        is set to do so.

        :param dict from unicode to object item: This is a dict representation of the item passed in to add_item
        :return: ignored
        """


class FileBasedMixin(DiscreteTask):
    base_file_name: Text

    # @abstract
    def extension(self):
        pass

    @property
    def temp_path(self):
        return "{}/{}_{}.{}".format(self.config.temp_dir("outbound"), self.base_file_name,
                                    self.process_manager.get_run_discriminator(),
                                    self.extension())

    @property
    def stream_path(self):
        return "{}/{}_{}.{}".format(self.config.temp_dir("outbound"), self.base_file_name,
                                    self.process_manager.get_run_discriminator(),
                                    "stream")
