from dataclasses import asdict
from itertools import chain
from typing import List

from mongoengine import fields


class GenericJsonSerializer(BaseSerializer):
    record_root = fields.StringField(default="records")

    def initialize(self):
        self.json_dest = codecs.open(self.stream_path, "wb")
        self.json_dest.org_write = self.json_dest.write
        self.json_dest.write = lambda d: self.json_dest.org_write(d.encode())
        root = {"version": "1.0.0-Draft",
                "run_date": str(datetime.datetime.now().date()),
                "transaction_id": self.process_manager.transaction_id,
                "instance": self.config.client_key
                }
        initial_json = json.dumps(root, cls=ComplexEncoder)[:-1]
        self.json_dest.write(initial_json)
        self.json_dest.write(',"{}": ['.format(self.record_root))
        return True

    def fixed_write(self, s):
        if isinstance(s, str):
            s = s.encode()

    def serialize(self, item):
        item_dict = item.to_mongo()
        """:type: dict from unicode to object"""
        ignored_fields = set(["client_key", "transaction_id"])
        ignored_fields.update(DataCollection.ignored_fields(item.__class__, *DataCollection._ignored_fields))
        if self.exclude_pii:
            ignored_fields.update(item.pii_fields())
        for ignored_field in ignored_fields:
            item_dict.pop(ignored_field, None)

        if "enrollment" in item_dict:
            if not isinstance(item_dict["enrollment"], dict):
                item_dict["enrollment"] = item_dict["enrollment"].to_mongo()
            ignored_fields = DataCollection.ignored_fields(EmbeddedEnrollmentRecord, *DataCollection._ignored_fields)
            if self.exclude_pii:
                ignored_fields.update(EmbeddedEnrollmentRecord.pii_fields())
            for ignored_field in ignored_fields:
                item_dict["enrollment"].pop(ignored_field, None)
        return json.dumps(item_dict, cls=ComplexEncoder)

    def extension(self):
        return "json"

    def write_to_path(self, local_path):
        self.json_dest.write("]}")
        self.json_dest.close()
        shutil.move(self.stream_path, local_path)
