import csv


class MSSQL(csv.excel):
    quoting = csv.QUOTE_NONNUMERIC


csv.register_dialect("mssql", MSSQL)


class MSSQLEscaped(csv.excel):
    quoting = csv.QUOTE_NONNUMERIC
    escapechar = "\\"
    doublequote = False


csv.register_dialect("mssql-escaped", MSSQLEscaped)


class Pipe(csv.excel):
    quoting = csv.QUOTE_NONE
    delimiter = '|'
    escapechar = "\\"


csv.register_dialect("pipe", Pipe)
