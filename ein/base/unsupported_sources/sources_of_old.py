from collections import Mapping
from dataclasses import dataclass
from typing import Text

from ein.inbound.sources import EinDataSource, SourceQuery
from ein.integration.databases import QueryBasedIntegration
from ein.integration.ftp import SftpConnection




@dataclass
class SftpIntegration(FileBasedSystemIntegration):
    path_to_directory: Text = None
    archive_bucket: Text = None
    connection_config: Mapping[Text, Text] = None
    connection_config_key: Text = None
    srv: SftpConnection = None

    @property
    def s3_archive(self):
        return boto3.resource('s3',
                              region_name='us-east-1',
                              aws_access_key_id='AKIAI5BD6SD7Y3I5ULIQ',
                              aws_secret_access_key='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')

    @property
    def connection_params(self):
        if self.connection_config_key is not None:
            return getattr(self._config, self.connection_config_key, self.connection_config)
        return self.connection_config

    @property
    def remote_files(self):
        return self.srv.listdir()

    # @abstract
    def get_archive_path(self, file_name):
        """the proper archive path for this run. Institution key is already added so it is not required to add.
        This does not include the file name. If it is desired to override the file name (ie change it from the original
        name) override archival_file_name

        :return: unicode
        """

    def __post_init__(self):
        super().__init__()
        self._srv = None
        """:type: SftpConnection"""

    def initialize(self):
        self.connect()

    def pull_data(self):
        download_path = self.get_download_dir()
        file_pulled = False
        for remote_file in self.remote_files:
            if remote_file == "archive":
                continue
            if self.should_process(remote_file, archived=False):
                local_file = os.path.join(download_path, remote_file)
                self.get_file(remote=remote_file, local=local_file)
                file_pulled = True
        if not file_pulled:
            logger.warn("Nothing found searching archive...")
            download_path = self.pull_off_archive()
            if download_path is None:
                logger.warn("File not available. Sleeping for 1 minute an checking again")
                time.sleep(60)
                return self.pull_data()
        return download_path

    def pull_off_archive(self):
        download_path = self.get_download_dir()
        bucket = self.s3_archive.Bucket(self.archive_bucket)
        file_found = False
        for f_obj in list(bucket.objects.filter(Prefix=self.get_archive_file_path('')))[1:]:
            if self.should_process(f_obj, archived=True):
                file_found = True
                logger.info("Found file %s, size %s. Starting download...", f_obj.key, f_obj.size)
                local_filename = f_obj.key.replace('/', '_')
                if not os.path.isfile(download_path + local_filename):
                    bucket.download_file(f_obj.key, download_path + local_filename)
                else:
                    logger.warning("File already exists locally")
        if not file_found:
            return None
        return download_path

    def load_complete(self):
        self.close()

    def connect(self):
        if getattr(self, "_srv", None) is None:
            self.srv = SftpConnection(**self.connection_params)
        self.srv.chdir(self.path_to_directory)

    def get_file(self, remote="", local=""):
        self.srv.get(self.path_to_directory + remote, localpath=local)

    def close(self):
        self.srv.close()
        del self.srv

    def archival_file_name(self, file_name):
        return file_name

    def get_archive_file_path(self, file_name):
        return os.path.join(self.client_key.lower(), self.get_archive_path(file_name),
                            self.archival_file_name(file_name))

    def archive_file(self, file_name):
        download_file = self.get_download_dir() + file_name
        file_info = os.stat(download_file)
        if file_info is None:
            # When file info is none that means the file is either 1) unobtainable or 2) (more likely) already archived
            return
        archive_file_path = self.get_archive_file_path(file_name)
        logger.info("Archiving {}, to {}".format(download_file, archive_file_path))
        s3obj = self.s3_archive.Object(self.archive_bucket, archive_file_path)
        try:
            s3obj.put(Body=open(download_file, 'rb'))
            self.delete_source_file(file_name)
        except Exception as ex:
            logger.exception(ex)
            logger.warning("File not archived. ")

    def delete_source_file(self, file_name):
        self.srv.remove(self.path_to_directory + file_name)

    def should_process(self, remote_file, archived=False):
        """ By default all files found on the FTP path will be processed if the directory is shared by
        multiple different files should_process can be overridden to determine if the file should be downloaded or not.

        :param unicode remote_file: path to the remote file that will be downloaded and processed
        :param boolean archived: a boolean to distinguish if the file is being pulled off archive or not
        :return: boolean
        """
        return True


class MongoSystemIntegration(QueryBasedIntegration):
    def processor_factory(self):
        return MongoQueryProcessor(self.mapping)

    def load_data(self, pulled_data):
        self._processor.query_set = pulled_data
        self._processor.process()

    class MongoQueryProcessor(Processor):
        def __init__(self, *args, **kwargs):
            self.query_set = kwargs.pop("query_set", None)
            super(MongoQueryProcessor, self).__init__(*args, **kwargs)
            self._source = None

        @property
        def source(self):
            return json.dumps((self._source or str(self.query_set._query))).replace("$", "__")

        @source.setter
        def source(self, v):
            self._source = v

        def count(self, iter):
            return iter.count()

        def open_data(self):
            pass

        def extract_headers(self, row):
            """ Headers for the row

            :param mongoengine.Document row: a document
            :return:
            """
            fields = dict((k, k) for k in list(row._data.keys()) if k not in DataCollection._ignored_fields + ['id'])
            return fields

        @property
        def iter(self):
            """

            :return: List
            """
            return self.query_set


class OracleQuery(SourceQuery):
    parameters: Mapping[Text, Text]

    def print(self):
        logger.info(re.sub(":([A-Za-z_]+[0-9]+)", r"'{\1}'", self.source_query).format(**self.parameters))

    def execute(self, cursor):
        self.print()
        try:
            cursor.execute(self.source_query, **self.parameters)
        except BaseException:
            logger.error(self.source_query)
            logger.error(json.dumps(self.parameters))
            query = self.source_query
            """:type: unicode"""
            for key, value in list(self.parameters.items()):
                query = query.replace(":" + key, value)
            # logger.error(query)
            raise


class DirectQueryProcessor(object):
    pass


@dataclass
class RelationalDatabaseIntegration(EinDataSource):
    _retry_attempts: int = 0

    # @abstract_property
    def cursor(self):
        """ Returns a context for a cursor to select data

        :return:
        """

    def processor_factory(self):
        return DirectQueryProcessor(
            system_integration=self,
            source_mapping=self.mapping)

    def pull_data(self):
        """Here pull_data will return a collection of queries that will be executed in load_data"""
        return self.queries

    def load_data(self, pulled_data):
        try:
            from pyodbc import Error as ExpectedError
        except ImportError:
            ExpectedError = BaseException

        try:
            with self.cursor as cur:
                self.processor.cursor = cur
                for q in pulled_data:
                    self._query = q
                    self.processor.process()
        except ExpectedError as ex:
            (error_code, error_message) = ex.args
            if "deadlock" in error_message or "prelogin failure" in error_message:
                logger.warn(error_message)
                if self._retry_attempts < 5:
                    self._retry_attempts += 1
                    time.sleep(5)
                    self.load_data(pulled_data)
                else:
                    raise
            else:
                raise


class AthenaSystemIntegration(EinDataSource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.query = self._queries = None

    def initialize(self):
        self.query = None
        self._queries = None

    # @abstract
    def make_query(self):
        """ Returns a tuple containing the query and the parameters of the query

        :rtype : tuple
        :return SourceQuery: a query and parameters
        """
        pass

    def make_queries(self):
        """ Returns a tuple containing the query and the parameters of the query

        :rtype : tuple
        :return tuple: a query and parameters
        """
        return [self.make_query()]

    @property
    def queries(self):
        if self._queries is None:
            self._queries = self.make_queries()
        return self._queries

    def processor_factory(self):
        return AthenaProcessor(self.mapping, system_integration=self)

    def pull_data(self):
        return self.queries

    def load_data(self, pulled_data):
        try:
            with self.cursor as cur:
                self._processor.cursor = cur
                for q in pulled_data:
                    self.query = q
                    self._processor.process()
        except BaseException as e:
            logger.error("error loading data", e)
            raise


class RestSystemIntegration(EinDataSource):
    @property
    def dialect(self):
        """The Dialect used by the  client usually. If you use the @maps decorator in the mapping class this will
        be implemented for you.

        :return:
        """
        return JSON()

    @property
    def client(self) -> ServiceClient:
        return self._processor

    # @abstract
    def client_factory(self, config) -> ServiceClient:
        raise NotImplemented("Implementations of the RestIntegration need to return the proper client")

    def load_data(self, client: ServiceClient):
        self._processor.process()
        logger.debug("Closing Processor down")

    def processor_factory(self):
        processor = self.client_factory()
        processor.init_processor(source_mapping=self.mapping, dialect=self.dialect)
        return processor
