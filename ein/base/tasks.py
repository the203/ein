import uuid
from dataclasses import dataclass
from types import SimpleNamespace
from typing import Any

from compose.exceptions import ComposeError
from ein import logger
from ein.runtime.context import job_context

class TaskError(ComposeError):
    pass

class DiscreteTask(object):
    """Building block of Ein. Most Servises and Sources will be a DiscreteTask which is little more than
    saying. It is callable, and anything needed to execute is inject, or set, before call is made

    """

    def __call__(self):
        try:
            self.main()
        except TypeError as ex:
            raise TaskError(f"Task call failed", location=self) from ex
        except (Exception, BaseException) as ex:
            # Often times internal errors start with a string reference to an object, to improve locating the issue
            # we'll parse the string and see if we can find it
            # TODO: try to figure this out if we can
            """quoted_items = re.compile("'([^']*?)'")
            exec_info = sys.exc_info()
            kls = None
            try:
                cls_name = quoted_items.findall(str(ex))[0]
                for frame, ln_no in traceback.walk_tb(exec_info[2]):
                    kls = frame.f_globals.get(cls_name, None)
                    if kls is not None:
                        break
            except BaseException:
                print("Error handling error")"""
            raise TaskError(f"Task Failure: {self}", location=self) from ex

        self.complete()

    def complete(self):
        job_context.complete(self)

    def main(self):
        raise NotImplementedError("")



class NullTask(DiscreteTask):
    def initialize(self):
        pass

    def main(self):
        pass


@dataclass
class DataItemPostProcessTask(object):
    priority: int = 7

    @staticmethod
    def add_field_to_document(document_cls, name, field):
        """ This is a way to add full fledged members to a document class defined elsewhere. It does use functionality
        quite a bit of "private" properties on the document's class so it could break on version change of mongoengine.

        :param document_cls:
        :param name:
        :param field:
        :return:
        """
        if name in document_cls._fields_ordered:
            return
        field.name = name
        field.db_field = name
        document_cls._fields[name] = field
        document_cls._db_field_map[name] = name
        document_cls._reverse_db_field_map[name] = name
        document_cls._fields_ordered += (name,)
        setattr(document_cls, name, field)

    def data_names(self):
        return []

    def cares_about(self, data_source):
        """

        :param data_source:
        :return: boolean
        """
        return data_source.name() in self.data_names

    def update_item(self, item, source):
        """ performs some sort of specific task to an item.

        :param DataCollection item:
        :return:
        """
        raise NotImplementedError("Update and item")

    def augment_data_type(self, cls):
        pass
