from dataclasses import dataclass, field
from typing import List, Optional

from compose.extras import clsname
from ein import logger
from ein.base.tasks import DiscreteTask
from ein.runtime.context import job_context
from ein.util import obj_location


@dataclass
class BaseService(DiscreteTask):
    _executed_tasks: List = field(init=False, default_factory=list)
    _current_task: DiscreteTask = field(init=False, default=None)

    @property
    def _name(self):
        return self.__class__.__name__.replace("Service", "")

    def execute(self, **kwargs):
        try:
            job_context.running(self)
        finally:
            try:
                self.always()
            except (Exception, BaseException) as ex:
                job_context.error(self, ex)
        try:
            self.run(**kwargs)
            logger.info(f"{clsname(self)} executed: {','.join(self._executed_tasks or ['none reported'])}")
        except (Exception, BaseException) as ex:
            job_context.error(self, ex)
        job_context.complete(self)

    def always(self):
        pass

    def current_task(self):
        return self._current_task

    def run(self) -> Optional[bool]:
        """
            Implementing classes should put the organization of however the sercvice is used in run.
            It is the primary interface to do whatever the service is created to do.

            :return: Boolean (optional) to indicate successful run. None == True in this case
        """
        pass

    def run_task(self, task, method="main"):
        """Runs a task, and if given a specific method on the task

        :param DiscreteTask task:   system_integration
        :param function or unicode method:  task
        :return: None

        """
        self._current_task = task
        job_context.job.run_task(task, method)


class NullService(BaseService):
    def execute(self, controller=None, config=None, **kwargs):
        logger.warning("Null Service Configured. Nothing is running for this task")
