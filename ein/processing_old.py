import codecs
import csv
import datetime
import json
import re
from mmap import mmap, ACCESS_READ

from ein.inbound.data import MongoDataCollection
from datamapping import mime_csv, JSON
from ein.exceptions import BadEntryException
from ein.runtime.context import job_context



class Processor(object):
    pass


class FileProcessor(Processor):
    def __init__(self, *args, dialect=mime_csv, **kwargs):
        super(FileProcessor, self).__init__(*args, **kwargs)
        self._headings = None

    @property
    def file_path(self):
        return self._file_path

    @file_path.setter
    def file_path(self, value):
        self._file_path = value

    @property
    def source(self):
        return self.file_path

    @property
    def iter(self):
        return self.feed_reader

    def open_data(self):
        self.datafile = codecs.open(self.file_path, 'rb', encoding=self.dialect.encoding, errors='ignore')
        self.feed_reader = self.get_iter(self.datafile, self.dialect)

    def extract_headers(self, row):
        if self._headings is None and self.dialect.firstrowheaders:
            self._headings = dict()
            for header in row:
                self._headings[re.sub('[^0-9A-Za-z]', "_", header.lower())] = len(self._headings)
            raise BadEntryException()
        return self._headings

    def get_iter(self, datafile, dialect):
        if hasattr(dialect, "sheet_index"):
            book = open_workbook(file_contents=mmap(datafile.fileno(), 0, access=ACCESS_READ))
            sheet = book.sheet_by_index(dialect.sheet_index)
            """
                return (sheet.row_values(row) for row in range(sheet.nrows))
            """
            data = []
            for row in range(sheet.nrows):
                row_cells = sheet.row_slice(row)
                row_values = []
                for cell in row_cells:
                    assert isinstance(cell, Cell)
                    cell_value = cell.value
                    if cell.ctype == 3:
                        cell_value = datetime.datetime(*xldate_as_tuple(cell_value, book.datemode)).isoformat()
                    if cell.ctype == 4:
                        cell_value = str(True if cell_value == 1 else False)
                    row_values.append(cell_value)
                data.append(row_values)
            return data.__iter__()
        else:
            # added replace for null bytes in case of empty line in file, thanks Liberty
            # TODO: loop/replace probably belongs in utf_8_encoder, this might be forcing the entire CSV into memory
            return csv.reader(utf_8_encoder(x.replace('\0', '') for x in datafile), dialect=self.dialect)
            # return csv.reader(utf_8_encoder(datafile), dialect=self.dialect)

    def count(self, iter):
        return len(iter)

    def finalize(self):
        self.datafile.close()


class ServiceClient(Processor):
    @property
    def config(self):
        return job_context.config

    def __init__(self, *args, **kwargs):
        """ This is here incase an implementation of ServiceClient calls super.init We don't want to
        propegrate the call to the super, Processor, the processor factory will handle this for Rest integrations.

        :param args:
        :param kwargs:
        """

    def init_processor(self, source_mapping, dialect=JSON, cls=None, process_manager=None, source_system=None):
        super(ServiceClient, self).__init__(source_mapping, dialect, cls, process_manager, source_system)

    @property
    def data(self):
        """ Returns an array of data points.

        :return:
        """

    # #@abstract
    def call(self):
        """ Used to call the service.

        :return:
        """

    @property
    def iter(self):
        """

        :return: List
        """
        dialect = self.dialect
        path = getattr(dialect, 'path', '')
        if path == '':

            return self.data
        else:
            data = self.data
            for branch in path.split('.'):
                data = data.get(branch, {})
            return data

    def open_data(self):
        self.call()

    def extract_headers(self, row):
        return dict((key, key) for key in self.data[0].keys())

    def count(self, iter):
        return len(self.data)


class JsonProcessor(Processor):
    def __init__(self, *args, **kwargs):
        super(JsonProcessor, self).__init__(*args, **kwargs)
        self._source = None

    @property
    def file_path(self):
        return self._file_path

    @file_path.setter
    def file_path(self, value):
        self._file_path = value

    @property
    def source(self):
        return self.file_path

    @property
    def iter(self):
        data = json.load(self.datafile)
        if self.dialect.root == "NA":
            return data
        else:
            if self.dialect.element == "NA":
                return data[self.dialect.root]
            else:
                return data[self.dialect.root][self.dialect.element]

    @source.setter
    def source(self, v):
        self._source = v

    def count(self, iter):
        return iter.count()

    def open_data(self):
        self.datafile = codecs.open(self.file_path, encoding=self.dialect.encoding)

    def finalize(self):
        self.datafile.close()

    def extract_headers(self, row):
        """ Headers for the row

        :param json row: dictionary object
        :return:
        """
        fields = dict((key, key) for key in row if key not in MongoDataCollection._ignored_fields + ['id'])
        return fields


class AthenaProcessor(Processor):
    def __init__(self, *args, **kwargs):
        self._system_integration = kwargs.pop("system_integration", None)
        super(AthenaProcessor, self).__init__(*args, **kwargs)
        self.cursor = None

    @property
    def system_integration(self):
        return self._system_integration

    def source(self):
        return "AWS Athena"

    def open_data(self):
        """
        system_integration is most likely going to be a AthenaSystemIntegration
        query is a SourceQuery defined in ...
        cursor is Athena client wrapper instance
        :return:
        """
        self.system_integration.query.execute(self.cursor)

    def extract_headers(self, row):
        return self.cursor.headers

    def iter(self):
        return self.cursor.results

    def count(self, iter):
        return len(self.cursor.results)



