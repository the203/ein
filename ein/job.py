import sys
import traceback
import uuid
from contextlib import redirect_stdout
from typing import List, Any, cast, Text, MutableMapping

from mongoengine import Document, fields

from compose.exceptions import ComposeError
from compose.extras.undef import Undefined
from compose import clsname
from ein.argparsercontrib import CommandArg, CommandFlag
from ein.base.tasks import DiscreteTask
from ein.exceptions import ProcessCompletedPreviously


from ein import logger


class JobState(Document):
    current_step = fields.StringField()
    start_time = fields.DateTimeField()
    state = fields.StringField()
    job_id = fields.StringField()
    job_name = fields.StringField()


class EinJob(object):

    @property
    def steps(self) -> List['JobStep']:
        self._steps = self.job_steps()
        yield from self._steps

    @property
    def partner(self):

        return self._context.client

    @classmethod
    def bind(cls, bundle):
        pass

    def __init__(self):
        self._current_step = cast(JobStep, None)

    def __call__(self, context, *args, **kwargs):
        self._context = context
        steps = self.steps
        if self._context.start_step:
            for i in range(0, int(self._context.start_step)):
                step = next(steps)
                logger.info(f"Skipping step: {step}")

        for step in steps:
            self._current_step = step
            try:
                step.run()
            except BaseException as ex:
                print_ex = ex
                locations = []
                if isinstance(ex, StepError):
                    print_ex = ex.__cause__
                    locations = ex.locations
                step_definition = traceback.format_stack(self._steps.gi_frame)[0]
                with redirect_stdout(sys.stderr):
                    print(f"Step Failed!")
                    traceback.print_exception(type(print_ex), print_ex, print_ex.__traceback__)
                    print("\n\nA step has failed for the job. The cause of the failure is printed above. \n"
                          "Ein is including some additional information to help track down the issue as "
                          "quickly as possible.\n")
                    print(f"Step  definition ", step_definition)
                    print("Additional locations that could be the source of the issue:")
                    for location in locations:
                        print(location)

                raise StepError("Step Failed") from None

    def run_task(self, task, method="main"):
        if self._current_step is None:
            self._context.raise_error("Task trying to be run before a Step. This is truly odd...")
        self._current_step.run_task(task, method)

    def job_steps(self) -> List['JobStep']:
        raise NotImplementedError("Job must do something")

    @property
    def args(self):
        basic = self.default_args()
        basic.update(self.job_args())
        return basic

    def job_args(self):
        return {}

    def default_args(self):
        return dict(
            client_key=CommandArg(required=True, prompt=True, opt_keys=("i", "client_key")),
            start_step=CommandArg(required=False, prompt=False, opt_keys=("start",)),
            local_override=CommandFlag(default=lambda cfg: cfg.is_local, prompt=lambda cfg: cfg.is_local,
                                       required=lambda cfg: cfg.is_local, opt_keys=("local"))
        )


class JobTracker(Document):
    job_id = fields.StringField(default=lambda: str(uuid.uuid4()))
    """:type: unicode"""
    child_ids = fields.ListField(fields.StringField())
    """:type: list of unicode"""
    complete = fields.BooleanField(default=False)
    """:type: boolean"""
    errored = fields.BooleanField(default=False)
    error = fields.StringField()
    stack_trace = fields.StringField()


class EinError(ComposeError):
    pass


class StepError(EinError):
    pass


class JobStep(DiscreteTask):
    """
        An executable step of a job.
    """

    def run_service(self, service: 'BaseService'):

        logger.info(f"{self.msg}. Sourced {clsname(service)}.")
        try:
            service.execute()
        except BaseException as ex:
            raise StepError(f"{clsname(service)} Failed to complete", location=service) from ex

    def run_task(self, task, method="main"):
        """Runs a task, and if given a specific method on the task

        :param BaseConfig config:  config
        :param DiscreteTask task:   system_integration
        :param function or unicode method:  task
        :return: None

        """
        logger.info(f"{self.msg}. Sourced {clsname(self._step)}.")
        if task is None:
            logger.warning("Null task tried to be run. This may be intentional.")
            return 0
        if isinstance(method, str):
            method = getattr(task, method, lambda: NotImplementedError(method))
        try:
            self._task_result = method()
            return 1

        except TypeError as ex:
            raise StepError(f"Task call failed", location=method) from ex
        except ProcessCompletedPreviously:
            logger.info(f"{task.__class__.__name__} completed Previously. It will be skipped")
            return

        except (Exception, BaseException) as ex:
            # Often times internal errors start with a string reference to an object, to impove locating the issue
            # we'll parse the string and see if we can find it
            # TODO: try to figure this out if we can
            """quoted_items = re.compile("'([^']*?)'")
            exec_info = sys.exc_info()
            kls = None
            try:
                cls_name = quoted_items.findall(str(ex))[0]
                for frame, ln_no in traceback.walk_tb(exec_info[2]):
                    kls = frame.f_globals.get(cls_name, None)
                    if kls is not None:
                        break
            except BaseException:
                print("Error handling error")"""
            raise StepError(f"Task Failure: {task}", location=task) from ex

    def __init__(self, executable, msg, *args, required=True, **kwargs: Any):
        """ The callable, a message to log when running.

        :param service:
        :param msg:
        :param required:
        """
        super().__init__(*args, **kwargs)
        self.msg = msg
        self._step = executable
        self._job = None
        self._must_run = required
        self._task_result = Undefined

    def __call__(self, **kwargs):
        self.run()

    def run(self):
        from ein.base.services import BaseService
        if self._step is None:
            logger.info(f"Step not provided Step '{self.msg}' will be skipped")
        if not isinstance(self._step, list):
            self._step = [self._step]
        execute_count: MutableMapping[Text, int] = dict()
        for step_part in self._step:
            if isinstance(step_part, BaseService):
                ex_type = "service"
                self.run_service(step_part)
            elif isinstance(step_part, DiscreteTask):
                if self.run_task(step_part):
                    ex_type = "task"
                else:
                    continue
            elif callable(step_part):
                self.run_task(self, method=step_part)
                ex_type = "method"
            else:
                logger.warning(f"{clsname(step_part)} is not um runnable?")
                continue
            execute_count[ex_type] = execute_count.get(ex_type, 0) + 1
        logger.info(f"Executed {execute_count}")
