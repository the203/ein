from typing import Type

from datamapping import *
from datamapping.source import _mapping_registry
from ein.inbound.data import MongoDataCollection
from ein.inbound.dialects import EinDialect
from ein.integration import SystemIntegration

_mapping_registry = _mapping_registry


class SourceMapping(SourceMapping):
    dialect: EinDialect = None


def mapping(to: Type[MongoDataCollection] = None,
            data_from: Type[SystemIntegration] = None,
            dialect=None):
    data_collection = mappable(to)

    def wrapper(mapping_cls):
        mapping_cls.dialect = dialect
        global _mapping_registry
        if data_from:
            _mapping_registry[data_from] = mapping_cls
        else:
            raise NotImplementedError()

        if data_collection:
            mapping_cls.target_collection = data_collection
        else:
            mapping_cls.target_collection = None
        return mapping_cls

    return wrapper
