import inspect
import logging
import logging.config
from datamapping.exceptions import *
__all__ = [
    'BadEntryException',
    'BaseLoggingException',
    'InvalidArgument'
]


class BaseLoggingException(BaseException):
    def __init__(self, msg, *args, **kwargs):
        frm = inspect.stack()[1]
        _name = inspect.getmodulename(inspect.getsourcefile(frm[0]))
        if _name is None:
            _name = frm[1]
        logger = logging.getLogger(_name)
        super(BaseLoggingException, self).__init__(msg, *args, **kwargs)
        logger.error(msg, *args)


class InvalidArgument(BaseLoggingException):
    pass


class ProcessStatusException(Exception):
    def __init__(self, process_stat=None, message=""):
        self.process_stat = process_stat
        super(ProcessStatusException, self).__init__(message)


class ProcessCompletedPreviously(ProcessStatusException):
    pass


class ProcessRunning(ProcessStatusException):
    pass


class ProcessOwnerConflict(ProcessStatusException):
    pass


class ProcessLocked(ProcessStatusException):
    pass
