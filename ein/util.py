from __future__ import print_function

import datetime
import errno
import os
from dataclasses import field
from typing import Text

from compose.extras import ascls



def force_zero(value):
    if value == "" or value is None:
        return 0
    return value


def ensure_path_exists(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise


def hash_prop(*hash_parts):
    for p in hash_parts:
        p.required = True

    def replace_func(f):
        def create_hash(document):
            f(document)
            m = hashlib.md5()
            for hash_part in hash_parts:
                h_value = getattr(document, hash_part.name)
                if h_value is None:
                    raise AttributeError("{} on {} can not be None".format(hash_part.name, document.__class__.__name__))
                if not isinstance(h_value, Text):
                    h_value = str(h_value)
                if isinstance(h_value, datetime.datetime):
                    h_value = h_value.isoformat()
                m.update(h_value.encode('ascii', 'xmlcharrefreplace'))
            return m.hexdigest()

        p = property(create_hash)
        return p

    return replace_func


def clean_key_for_mongo(key):
    return re.sub("[^a-zA-Z0-9_]", "_", key.lower())


def ensure_dir(d):
    if not os.path.exists(d):
        os.makedirs(d)
    return d


def proper_case(str):
    return str.title().replace("_", " ")


class BlockTimer(object):
    def __init__(self, spot, logger=None):
        self.logger = logger or self.printer
        self.msg = "Timing for {}: ".format(spot or "code block")

    def printer(self, msg):
        print(msg)

    def __enter__(self):
        self.start = time.clock()
        return self

    def __exit__(self, *args, **kwargs):
        self.end = time.clock()
        self.interval = self.end - self.start
        self.logger(self.msg + str(self.interval))


from mongoengine import fields
from mongoengine.base import BaseField



def convert_to(obj, cls):
    target_obj = cls()
    for field, value in iter_data(obj, cls=cls):
        setattr(target_obj, field, value)
    return target_obj


BAD_FEILDS = [
    "_cls"
]


def iter_data(obj, cls=None):
    if cls is None:
        cls = obj.__class__
    for field in dir(obj):
        if field not in BAD_FEILDS and isinstance(getattr(cls, field, obj._dynamic_fields.get(field)), BaseField):
            yield field, getattr(obj, field, None)


def url_converter(cls):
    class ImplicitConverter(MongoBasedConverter):
        _converter_for = cls

    return ImplicitConverter


try:
    from werkzeug.routing import BaseConverter
except ImportError:
    BaseConverter = object


class MongoBasedConverter(BaseConverter):
    _converter_for = None

    def to_python(self, value):
        return self._converter_for.objects.with_id(value)

    def to_url(self, obj):
        if isinstance(obj, self._converter_for):
            return obj.id
        if isinstance(obj, (basestring, unicode, str)):
            return obj


class IgnoreField(fields.DynamicField):
    pass


class IgnoreExtraData(object):
    """ With MongoEngine .9 Documents that are not made dynamic will raise exceptions if properties that are not
    defined on an object are passed in to the init. This class provided a work around for that.
    """
    meta = {"abstract": True}

    def __new__(cls, *args, **values):
        rejected_fields = []
        acceptable_keys = dir(cls)
        for k in list(values.keys()):
            if k not in acceptable_keys and k[0:2] != "__":
                rejected_fields.append(k)
                add_field_to_document(cls, k, IgnoreField())
        if len(rejected_fields) > 0:
            logger.debug("%s field(s) not on document definition", ", ".join(rejected_fields))
        obj = object.__new__(cls)
        return obj


def add_field_to_document(document_cls, name, field):
    """ This is a way to add full fledged members to a document class defined elsewhere. It does use functionality
    quite a bit of "private" properties on the document's class so it could break on version change of mongoengine.

    :param document_cls:
    :param name:
    :param field:
    :return:
    """
    if name in document_cls._fields_ordered:
        return
    field.name = name
    field.db_field = name
    document_cls._fields[name] = field
    document_cls._db_field_map[name] = name
    document_cls._reverse_db_field_map[name] = name
    document_cls._fields_ordered += (name,)
    setattr(document_cls, name, field)


import re


def make_user_id(item):
    """
        @type item: StudentActivity
    """
    m = hashlib.md5()
    m.update(safe_for_md5(item.first_name))
    m.update(safe_for_md5(item.last_name))
    return m.hexdigest()


def make_activity_id(item):
    """
        @type item: StudentActivity
    """
    m = hashlib.md5()
    m.update(safe_for_md5(item.classification))
    m.update(safe_for_md5(item.activity_label))
    return m.hexdigest()


def safe_for_md5(value):
    if not isinstance(value, str):
        value = str(value, "UTF-8")
    return value.encode("UTF-8")


def convert_100_base_score(score):
    if score == "":
        score = 0.0
    score = float(score)
    return score / 100.0


def name_for_lookup(name):
    return re.sub("[^a-zA-Z]", "", (name or "").lower())


def filter_by_type(type, collection):
    """Grab all the objects of a certain type from a collection and return in dictionary format.
    {
     "ObjectName": Object
    }

    :param type: object
    :param collection: list
    :return:
    """

    return dict((ds.__class__.__name__, ds)
                for ds in collection if isinstance(ds, type))


def to_bool(s):
    if s is None:
        return False
    if s is True or s == 1 or str(s).lower() in ("true", "yes"):
        return True
    return False


import hashlib

import time

from ein import logger


def fullname(o):
    module = o.__class__.__module__
    if module is None or module == str.__class__.__module__:
        return o.__class__.__qualname__
    return module + '.' + o.__class__.__qualname__


def resolve_config_name(o):
    if o.__class__.__qualname__ == "Flask":
        return o.config["NAME"]
    return fullname(o)


class KeyCleaner(object):
    # Use these definitions if you plan on using a string as a key in a dict or map that may contain . or $ which
    # are not allowed
    @staticmethod
    def encrypt_title(key):
        """
        Replaces . and $ with ^ and `
        :param key: unicode
        :return: unicode
        """
        return key.replace('.', '^').replace('$', '`')

    @staticmethod
    def decrypt_title(key):
        """
        Replaces ^ and ` with . and $ (reverts back to original form)
        :param key: unicode
        :return: unicode
        """
        return key.replace('^', '.').replace('`', '$')


class classproperty(property):
    def __get__(self, obj, objtype=None):
        return super(classproperty, self).__get__(objtype)

    def __set__(self, obj, value):
        super(classproperty, self).__set__(type(obj), value)

    def __delete__(self, obj):
        super(classproperty, self).__delete__(type(obj))


def defaultnamespace(value_or_factory):
    factory = value_or_factory
    if not callable(factory):
        factory = lambda item: value_or_factory

    class DefaultNameSpace(object):
        def __getattribute__(self, item):
            return CallableString(factory(item))

    return DefaultNameSpace()


class CallableString(str):
    def __call__(self, *args, **kwargs):
        return self


def make_callable(thing):
    c = type('Callable' + clsname(thing), (ascls(thing),), {
        '__call__': lambda s, *a, **k: s(*a, **k)
    })
    return c(thing)


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('%r  %2.2f ms' % \
                  (method.__name__, (te - ts) * 1000))
        return result

    return timed


def create_hash(values):
    import datetime
    m = hashlib.md5()
    for key, h_value in values.items():
        if h_value is None:
            raise AttributeError(f"{key}  can not be None")
        if not isinstance(h_value, str):
            h_value = str(h_value)
        if isinstance(h_value, datetime.datetime):
            h_value = h_value.isoformat()
        m.update(h_value.encode('ascii', 'xmlcharrefreplace'))
    return m.hexdigest()


def obj_location(obj):
    import inspect
    location = ascls(obj)
    return f'File "{inspect.getsourcefile(location)}", ' \
        f'line {inspect.findsource(location)[1] + 1}, in {clsname(location)}'


def a_list():
    return field(default_factory=list)


from functools import reduce


def flatten(d, pref='', sep="."):
    return (reduce(
        lambda new_d, kv: (isinstance(kv[1], dict)
                           and {**new_d, **flatten(kv[1], pref + kv[0] + sep)}
                           or {**new_d, pref + kv[0]: kv[1]}),
        d.items(),
        {}
    ))
